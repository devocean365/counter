# Counter

A simple counter app bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and Redux. It allows to increment the count stored in redux storage and navigate to counter-list page, which shows the list of all counts that have been hit so far.

## Installation, tests & build

```sh
git clone git clone git@bitbucket.org:devocean365/counter.git
cd counter
yarn install
yarn start
```

To run tests type:
```sh
yarn test
```

To build the app type:
```sh
yarn build
```