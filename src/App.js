import React, { Component } from 'react';
import './App.css';
import Counter from './components/Counter';
import CounterList from './components/CounterList';
import {BrowserRouter as Router, Route} from 'react-router-dom'


class App extends Component {
  
  render() {

    return (
      <div className="App">
        <header className="App-header">
          <div className="container">
            <h3 className="App-title">Counter build with Create React App & Redux</h3>
          </div>
        </header>
        <Router>
          <div>
            <Route exact path="/" render={() => <Counter {...this.props} />} />
            <Route path="/counter-hitlist" render={() => <CounterList counterList={this.props.counterList} />} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;