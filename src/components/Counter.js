import React from 'react';
import {Link} from 'react-router-dom';
import {Button, Nav, NavItem, NavLink} from 'reactstrap'

export default ({count, onIncrement, onDecrement}) => (
  <div className="Counter-App container">
    <Nav pills>
    	<NavItem>
    		<NavLink tag={Link} to="/counter-hitlist">Counter Hitlist</NavLink>
    	</NavItem>
    </Nav>
    <div className="App-Buttons">
	    <Button onClick={onIncrement}>+</Button>
	    <Button onClick={onDecrement}>-</Button>
	  </div>
	  <h1 className="App-title">Count is: {count}</h1>
  </div>
)