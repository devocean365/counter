import React from 'react';
import {Link} from 'react-router-dom';
import {ListGroup, ListGroupItem, Nav, NavItem, NavLink} from 'reactstrap';

export default ({counterList}) => (
  <div className="container">
  	<Nav pills>
    	<NavItem>
    		<NavLink tag={Link} to="/">{"< Back"}</NavLink>
    	</NavItem>
    </Nav>
    <h1 className="App-title">{counterList && "These counts have already been hit"}</h1>
    <ListGroup>{counterList && counterList.map((count) => <ListGroupItem key={count}>{count}</ListGroupItem>)}</ListGroup>
  </div>
)