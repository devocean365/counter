import reducer from './counter'

describe('Counter reducer', () => {

	test('returns a state object', () => {
		const result = reducer(undefined, {type: "ANYTHING"})
		expect(result).toBeDefined()
	})

	test('increments counter', () => {
		const startState = {count: 8}
		const expectedState = {count: 9}
		const action = { type: 'INCREMENT' }
		const result = reducer(startState, action)
		expect(result.count).toEqual(expectedState.count)

	})

	test('decrements counter', () => {
		const startState = {count: 1}
		const expectedState = {count: 0}
		const action = { type: 'DECREMENT' }
		const result = reducer(startState, action)
		expect(result.count).toEqual(expectedState.count)

	})

	test('incremented counter less or equal to 10', () => {
		const startState = {count: 10}
		const maxCount = 10
		const action = { type: 'INCREMENT' }
		const result = reducer(startState, action)
		expect(result.count).toBeLessThanOrEqual(maxCount)

	})

	test('decremented counter more or equal to 0', () => {
		const startState = {count: 0}
		const minCount = 0
		const action = { type: 'INCREMENT' }
		const result = reducer(startState, action)
		expect(result.count).toBeGreaterThanOrEqual(minCount)

	})

})