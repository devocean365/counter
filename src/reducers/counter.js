const initialState = {
	count: 0,
	counterList: [0],
}

export default (state = initialState, action) => {
	const currentStateCount = state.count;
	const currentStateCounterList = state.counterList || [0];

	switch(action.type) {
		case 'INCREMENT':
			const incrementedCount = Math.min(10, currentStateCount + 1);
			const incrementedCounterList = currentStateCounterList.indexOf(incrementedCount) === -1 ? [...currentStateCounterList, incrementedCount] : currentStateCounterList;
			const incrementedCountState = Object.assign({}, state, {count: incrementedCount, counterList: incrementedCounterList });
			return incrementedCountState;

		case 'DECREMENT':
			const decrementedCount = Math.max(0, currentStateCount - 1);
			const decrementedCounterList = currentStateCounterList.indexOf(decrementedCount) === -1 ? [...currentStateCounterList, decrementedCount] : currentStateCounterList;
			const decrementedCountState = Object.assign({}, state, {count: decrementedCount, counterList: decrementedCounterList});
			return decrementedCountState;

		default:
			return state;
	}
}