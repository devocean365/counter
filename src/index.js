import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css';

const incrementHandler = () => store.dispatch({type: "INCREMENT"})
const decrementHandler = () => store.dispatch({type: "DECREMENT"})

const render = () => {
	const state = store.getState();
	ReactDOM.render(<App count={state.count} counterList={state.counterList} onIncrement={incrementHandler} onDecrement={decrementHandler}/>, document.getElementById('root'));
	
}

render();

store.subscribe(render);
registerServiceWorker();