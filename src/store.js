import {createStore} from 'redux'
import reducer from './reducers/counter.js'

export default createStore(reducer)